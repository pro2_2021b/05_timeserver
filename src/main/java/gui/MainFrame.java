package gui;
import guiModel.*;
import model.*;

import javax.swing.*;

public class MainFrame extends JFrame
{
    JList jlist;
    LogListModel logListModel;

    public MainFrame()
    {
        setTitle("Time server");
        setVisible(true);
        setSize(600,800);

        jlist = new JList();
        // vytvoření testovacích dat
        Log log = new Log();
        log.addItem(new LogItem("IP1", null));
        log.addItem(new LogItem("IP2", null));
        log.addItem(new LogItem("IP3", null));
        // konec

        logListModel = new LogListModel(log);
        jlist.setModel(logListModel);
        add(jlist);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    public void AddLogItem(LogItem logItem)
    {
        System.out.println(logItem.toString());
        logListModel.addItem(logItem);
    }
}
