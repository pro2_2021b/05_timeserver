package guiModel;

import model.Log;
import model.LogItem;

import javax.swing.*;

public class LogListModel extends AbstractListModel
{
    Log log;

    public LogListModel(Log log)
    {
        this.log = log;
    }

    @Override
    public int getSize() {
        return log.getItemsCount();
    }

    @Override
    public Object getElementAt(int index) {
        return log.getItem(index);
    }

    public void addItem(LogItem logItem)
    {
        log.addItem(logItem);
        fireIntervalAdded(
                this,
                log.getItemsCount() -1,
                log.getItemsCount() -1 );
    }
}
