package app;

import gui.MainFrame;
import model.LogItem;

import javax.swing.*;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.time.*;

public class TestApp
{
    public static void main(String[] args) throws IOException {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run()
            {
                MainFrame mainFrame = new MainFrame();
                Thread thread = new Thread()
                {
                    @Override
                    public void run()
                    {
                        ServerSocket serverSocket = null;
                        try
                        {
                            serverSocket = new ServerSocket(180);
                            while (true)
                            {
                                Socket socketConnection = serverSocket.accept();
                                Thread thread = new Thread(){
                                    @Override
                                    public void run()
                                    {
                                        try
                                        {
                                            OutputStream outputStream = socketConnection.getOutputStream();
                                            PrintWriter printWriter = new PrintWriter(outputStream);
                                            LocalDateTime time = LocalDateTime.now();
                                            printWriter.println(time.toString());
                                            printWriter.close();
                                            socketConnection.close();
                                            mainFrame.AddLogItem(
                                                    new LogItem(
                                                            socketConnection.getInetAddress().toString(),
                                                            time));
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                };
                                thread.start();
                            }
                        }
                        catch (IOException e)
                        {
                            e.printStackTrace();
                        }
                    }
                };
                thread.start();
            }
        });
    }
}
