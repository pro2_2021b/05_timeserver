package model;

import java.time.LocalDateTime;

public class LogItem
{
    public LogItem(String client, LocalDateTime localDateTime)
    {
        this.client = client;
        this.localDateTime = localDateTime;
    }

    String client;
    LocalDateTime localDateTime;

    @Override
    public String toString()
    {
        return client + ", " + localDateTime;
    }
}
