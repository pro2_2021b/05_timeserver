package model;

import java.util.ArrayList;

public class Log
{
    ArrayList<LogItem> items = new ArrayList<>();
    String author;
    //DateTime start;

    public LogItem getItem(int index)
    {
        return items.get(index);
    }

    public int getItemsCount()
    {
        return items.size();
    }

    public void addItem(LogItem logItem)
    {
        items.add(logItem);
    }
}
